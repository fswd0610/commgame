﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CommGame.Data;

namespace CommGame.UI.Controllers
{
    public class HomeController : Controller
    {
        private CommGameEntities db = new CommGameEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Game()
        {
            
            return View(db.UnicodeCharacters.ToList());
        }

        public ActionResult Win()
        {
            return View();
        }
    }
}