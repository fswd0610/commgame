﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CommGame.Data;
using CommGame.UI.Models;
using System.Threading.Tasks;

namespace CommGame.UI.Controllers
{
    public class UnicodeCharactersController : Controller
    {

        private CommGameEntities db = new CommGameEntities();
        private SolButtModel stuff = SolButtModel.SolButt(new Data.CommGameEntities());


        //private static List<UnicodeCharacter> GetUnicodeCharacters(CommGameEntities db)
        //{
        //    List<UnicodeCharacter> UnicodeChars = new List<UnicodeCharacter>();
        //    UnicodeChars = db.UnicodeCharacters.ToList();

        //    List<UnicodeCharacter> ReturnList = new List<UnicodeCharacter>();

        //    for (int i = 0; i < 24; i++)
        //    {
        //        ReturnList.Add(UnicodeChars.ElementAt(new Random().Next(UnicodeChars.Count())));
        //        System.Threading.Thread.Sleep(40);
        //    }
        //    return ReturnList;
        //}

        //private static List<UnicodeCharacter> UniqueUnicodeChars()
        //{
        //    CommGameEntities db = new CommGameEntities();
        //    int maxLength = 12;
        //    int number = db.UnicodeCharacters.Count();
        //    List<UnicodeCharacter> UnicodeChars = new List<UnicodeCharacter>();
        //    while (UnicodeChars.Count < maxLength)
        //    {
        //        var temp = db.UnicodeCharacters.ElementAt(new Random().Next(number));
        //        if (UnicodeChars.Contains(temp))
        //        {

        //        }
        //        else
        //        {
        //            UnicodeChars.Add(temp);
        //        }
        //    }
        //    return UnicodeChars;
        //}


        public ActionResult Game()
        {
            return View(stuff);
        }
        // GET: UnicodeCharacters
        public ActionResult Index()
        {
            return View(db.UnicodeCharacters.ToList());
        }

        // GET: UnicodeCharacters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnicodeCharacter unicodeCharacter = db.UnicodeCharacters.Find(id);
            if (unicodeCharacter == null)
            {
                return HttpNotFound();
            }
            return View(unicodeCharacter);
        }

        // GET: UnicodeCharacters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UnicodeCharacters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CharID,CharHTML")] UnicodeCharacter unicodeCharacter)
        {
            if (ModelState.IsValid)
            {
                db.UnicodeCharacters.Add(unicodeCharacter);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(unicodeCharacter);
        }

        // GET: UnicodeCharacters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnicodeCharacter unicodeCharacter = db.UnicodeCharacters.Find(id);
            if (unicodeCharacter == null)
            {
                return HttpNotFound();
            }
            return View(unicodeCharacter);
        }

        // POST: UnicodeCharacters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CharID,CharHTML")] UnicodeCharacter unicodeCharacter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(unicodeCharacter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(unicodeCharacter);
        }

        // GET: UnicodeCharacters/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    UnicodeCharacter unicodeCharacter = db.UnicodeCharacters.Find(id);
        //    if (unicodeCharacter == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(unicodeCharacter);
        //}

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnicodeCharacter unicodecharacter = await db.UnicodeCharacters.FindAsync(id);
            if (unicodecharacter == null)
            {
                return HttpNotFound();
            }
            db.UnicodeCharacters.Remove(unicodecharacter);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // POST: UnicodeCharacters/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    UnicodeCharacter unicodeCharacter = db.UnicodeCharacters.Find(id);
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
