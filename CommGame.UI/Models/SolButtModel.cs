﻿using CommGame.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommGame.UI.Models
{
    public class SolButtModel
    {
        public List<UnicodeCharacter> ButtList { get; set; }
        public List<UnicodeCharacter> SolList { get; set; }

        public override string ToString()
        {
            string answer = "";
            foreach (var item in this.SolList)
            {
                answer += (item + " ");
            }
            return answer;
        }

        public static SolButtModel SolButt(CommGameEntities db)
        {
            SolButtModel bothLists = new SolButtModel();

            List<UnicodeCharacter> dbProxy = new List<UnicodeCharacter>();
            dbProxy = db.UnicodeCharacters.ToList();

            List<UnicodeCharacter> fullList = new List<UnicodeCharacter>();

            for (int i = 0; fullList.Count < 24; i++)
            {
                int temp = new Random().Next(dbProxy.Count());
                if (fullList.Contains(dbProxy.ElementAt(temp)))
                {

                }
                else
                    fullList.Add(dbProxy.ElementAt(temp));
                System.Threading.Thread.Sleep(40);
            }

            bothLists.ButtList = fullList;

            List<UnicodeCharacter> solList = new List<UnicodeCharacter>();
            foreach (var item in fullList)
            {
                solList.Add(item);
            }

            while (solList.Count > 12)
            {
                solList.Remove(solList.ElementAt(new Random().Next(solList.Count())));
                System.Threading.Thread.Sleep(40);
            }

            bothLists.SolList = solList;

            return bothLists;
        }
    }
}