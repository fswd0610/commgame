﻿let boxes = document.getElementsByClassName("box");

var boxesIndex;


var solutionList = [];

var problemList = [];

function getSolution() {

    let solution = document.getElementsByClassName('solVal');
    for (let i = 0; i < (solution).length; i++) {
        //console.log(solution[i].innerText);
        solutionList.push((solution[i].innerText))

    }
    console.log(solutionList);
}
function getProblem() {
    let problem = document.getElementsByClassName('probVal');
    problemList = [];
    for (let i = 0; i < (problem).length; i++) {
        //console.log(problem[i].innerText);
        problemList.push(problem[i].innerText)

    }
    console.log(problemList);
}

getSolution();
getProblem();
function getValue(i) {
    //$(this).css("cursor", "none");    
    $('.box').css('cursor', 'pointer');
    boxesIndex = i;
}

//for (var i = 0; i < boxes.length; i++) {
//    boxes[i].addEventListener("click", function(){
//        boxValue = boxes[i].getAttribute("value");
//    })
//}
const containers = document.getElementsByClassName('holder')
//containers.forEach(function (container) {   
//    container.addEventListener("dragover", dragover)
//    container.addEventListener("dragenter", dragenter)
//    container.addEventListener("drop", drop)
//})
for(const container of containers) {
    container.addEventListener("dragover", dragover)
    container.addEventListener("dragenter", dragenter)
    container.addEventListener("drop", drop)
}

function dragover(e) {    
    e.preventDefault()
}
function dragenter(e) {
    e.preventDefault()
}
function drop() {

    let boxValue = boxes[boxesIndex].innerText;
    let parentContainer = boxes[boxesIndex].parentElement;
    //console.log(parentContainer);
    let prevValue = $(this).html();
    let parent = $(this).parent();

    $(this).html(boxValue);




    //this.replaceWith(box)
    //var str = "Visit Microsoft!";
    //var res = str.replace("Microsoft", "W3Schools");
    // $(this).append(boxValue);
    //boxvalue = box thats taken
    //holder = box being dropped into

    //third variable = holders initial value

    //replace holder value with box value

    //replace boxvalue with third variable
    getProblem();
    if (solutionList.toString() == problemList.toString()) {
        alert("You win!")
    console.log("you win")
    }
}
$('body').on('drag', () => {
    $(this).css('cursor', 'none');
});





var pID;





//when page loads, check localStorage to see if the theme is set
if (localStorage.getItem('pID') != null) {
    pID = (localStorage.getItem('pID'));
}


if (pID == 1) {
    $('#solutionTable').hide();
}

if (pID == 2) {
    $('#buttons').hide();
}


$(function () {
    // Declare a proxy to reference the hub. 
    var chat = $.connection.chat;
    // Create a function that the hub can call to broadcast messages.
    chat.client.broadcastMessage = function (name, message) {
        // Html encode display name and message. 
        var encodedName = $('<div />').text(name).html();
        var encodedMsg = $('<div />').text(message).html();
        // Add the message to the page. 
        $('#discussion').append('<li><strong>' + encodedName
            + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
    };
    // Get the user name and store it to prepend to messages.
    $('#displayname').val(prompt('Enter your name:', ''));
    // Set initial focus to message input box.  
    $('#message').focus();
    // Start the connection.
    $.connection.hub.start().done(function () {
        $('#sendmessage').click(function () {
            // Call the Send method on the hub. 
            chat.server.send($('#displayname').val(), $('#message').val());
            // Clear text box and reset focus for next comment. 
            $('#message').val('').focus();
        });
    });
});