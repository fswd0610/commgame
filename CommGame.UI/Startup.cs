﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CommGame.UI.Startup))]
namespace CommGame.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
